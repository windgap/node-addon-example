#include <napi.h>
 
Napi::String Method(const Napi::CallbackInfo& info) {
	Napi::Env env = info.Env();
	Napi::String str= Napi::String::New(env, "hello "+info[0].As<Napi::String>().Utf8Value());
	return str;
}
 
Napi::Object Init(Napi::Env env, Napi::Object exports) {
	exports.Set(Napi::String::New(env, "hello"),
		Napi::Function::New(env, Method));
	return exports;
}
 
NODE_API_MODULE(hello,Init)