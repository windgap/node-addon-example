#![feature(core_intrinsics)]

// 需要切换rustup到nightly版本才能运行cargo run
// rustup toolchain list
// rustup default nightly
fn print_type_of<T>(_: T) {
    println!("{}", unsafe { std::intrinsics::type_name::<T>() });
}


fn main() {
    println!("Hello, world!");
    let place1 = "hello";
    let place2 = "hello".to_string(); // type `std::string::String`, which does not implement the `COPY` trait.
    // let place2 = "hello";
    print_type_of(&place2);

    let other = place1;
    println!("place1: {:?}", place1);
    println!("other: {:?}", other);
    let other2 = place2;
    println!("other2: {:?}", other2);

    print_type_of(&32.90);
    print_type_of(&place1);
}
